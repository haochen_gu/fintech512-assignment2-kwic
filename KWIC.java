import java.util.*;

public class KWIC {

    public ArrayList<String> read_ignore(ArrayList<String> list) {

        ArrayList<String> ignore_list = new ArrayList<>();

        for (int i=0; i<list.size(); i++) {
            if (list.get(i).equals("::")) {
                break;
            }
            else {
                ignore_list.add(list.get(i));
            }
        }

        return ignore_list;
    }

    public ArrayList<String> read_title(ArrayList<String> list) {

        ArrayList<String> title_list = new ArrayList<>();
        int count = 0;

        for (int i=0; i<list.size(); i++) {
            if (list.get(i).equals("::")) {
                count++;
                break;
            }
            else {
                count++;
            }
        }

        for (int i=count; i<list.size(); i++) {
            title_list.add(list.get(i));
        }

        return title_list;
    }

    public ArrayList<String> clean_data(ArrayList<String> title, ArrayList<String> ignore) {

        ArrayList<String> key_storage = new ArrayList<>();

        for (int i=0; i<title.size(); i++) {
            String temp = title.get(i);
            String[] splited = temp.split(" ");
            for (int j=0; j<splited.length; j++) {
                key_storage.add(splited[j].toUpperCase());
            }
        }

        for (int i=0; i<ignore.size(); i++) {
            for (int j=0; j<key_storage.size(); j++) {
                if ((ignore.get(i).toUpperCase()).equals(key_storage.get(j))) {
                    key_storage.remove(j);
                }
                else {
                    continue;
                }
            }
        }

        return key_storage;
    }

    public ArrayList<String> delete_duplicate(ArrayList<String> key_list) {

        ArrayList<String> no_double = new ArrayList<String>();

        for (String s : key_list) {
            if (!no_double.contains(s)) {
                no_double.add(s);
            }
        }
        return no_double;
    }

    public ArrayList<String> sort_list(ArrayList<String> no_double) {
        Collections.sort(no_double);
        return no_double;
    }

    // as there might multiple appearances of one keyword in a title, this function show how many times a key shows.
    public int key_num(String main_string, String sub_string) {
        int num_index = 0;
        num_index = main_string.split(sub_string, -1).length - 1;
        return num_index;
    }

    public ArrayList<String> output_list(ArrayList<String> sorted, ArrayList<String> title_list) {

        ArrayList<String> result = new ArrayList<String>();

        for (int i=0; i<sorted.size(); i++) {
            for (int j=0; j<title_list.size(); j++) {
                if ((title_list.get(j).toLowerCase()).contains(sorted.get(i).toLowerCase())) {

                    int num = key_num(title_list.get(j).toLowerCase(), sorted.get(i).toLowerCase());

                    //System.out.println(num);
                    if (num == 1) {
                        String out = title_list.get(j).toLowerCase();

                        int begin = out.indexOf(sorted.get(i).toLowerCase());
                        String final_out = out.substring(0, begin) + sorted.get(i) +
                                out.substring(begin+sorted.get(i).length());

                        result.add(final_out);
                    }

                    // solve the problem that one key word can show multiple times in one title
                    if (num > 1) {
                        String out = title_list.get(j).toLowerCase();
                        String temp = out;
                        int sum_begin = 0;
                        for (int k=0; k<num; k++) {
                            int begin = out.indexOf(sorted.get(i).toLowerCase());
                            int extra = k * sorted.get(i).length();
                            sum_begin = sum_begin + begin;

                            String final_out =  temp.substring(0, sum_begin+extra) + sorted.get(i) +
                                    out.substring(begin+sorted.get(i).length());

                            result.add(final_out);

                            out = out.substring(begin+sorted.get(i).length());

                        }
                    }
                }
                else {
                    continue;
                }
            }

        }

        return result;

    }


    public static void main(String[] args) {

        Scanner sc =  new Scanner(System.in);
        int countSeperator = 0;
        ArrayList<String> input = new ArrayList<String>();

        while (true) {
            String data = sc.nextLine();
            if (data.equals("")) {
                break;
            }
            if (data.equals("::")) {
                countSeperator++;
            }
            input.add(data);
        }

        sc.close();

        if (countSeperator != 1) {
            System.out.println("The input data you enter has wrong format.");
        }

        ArrayList<String> title_list = new ArrayList<String>();
        ArrayList<String> ignore_list = new ArrayList<String>();

        // read the data from the input both words to ignore and title
        ignore_list = new KWIC().read_ignore(input);
        title_list = new KWIC().read_title(input);

        // get the keywords list
        ArrayList<String> key_list = new ArrayList<String>();
        key_list = new KWIC().clean_data(title_list, ignore_list);

        // delete the duplicate word in the key list
        ArrayList<String> no_duplicate_key = new ArrayList<String>();
        no_duplicate_key = new KWIC().delete_duplicate(key_list);

        // sort the key list alphabetically
        ArrayList<String> sorted_list = new ArrayList<String>();
        sorted_list = new KWIC().sort_list(no_duplicate_key);

        ArrayList<String> output = new ArrayList<String>();
        output = new KWIC().output_list(sorted_list, title_list);

        for (int i=0; i<output.size(); i++) {
            System.out.println(output.get(i));
        }



    }

}

