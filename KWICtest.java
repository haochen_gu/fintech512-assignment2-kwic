import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


class KWICtest {

    // test the read of ignore
    @Test
    void test_ignore_list() {
        ArrayList<String> list = new ArrayList<>();
        list.add("is");
        list.add("the");
        list.add("of");
        list.add("and");
        list.add("as");
        list.add("a");
        list.add("but");
        list.add("::");
        list.add("Descent of Man");
        list.add("The Ascent of Man");
        list.add("The Old Man and The Sea");
        list.add("A Portrait of The Artist As a Young Man");
        list.add("A Man is a Man but Bubblesort IS A DOG");

        ArrayList<String> title_list = new ArrayList<>();
        title_list.add("is");
        title_list.add("the");
        title_list.add("of");
        title_list.add("and");
        title_list.add("as");
        title_list.add("a");
        title_list.add("but");
        assertEquals(new KWIC().read_ignore(list), title_list);
    }

    // test the read of title
    @Test
    void test_title_list() {
        ArrayList<String> list = new ArrayList<>();
        list.add("is");
        list.add("the");
        list.add("of");
        list.add("and");
        list.add("as");
        list.add("a");
        list.add("but");
        list.add("::");
        list.add("Descent of Man");
        list.add("The Ascent of Man");
        list.add("The Old Man and The Sea");
        list.add("A Portrait of The Artist As a Young Man");
        list.add("A Man is a Man but Bubblesort IS A DOG");

        ArrayList<String> title_list = new ArrayList<>();
        title_list.add("Descent of Man");
        title_list.add("The Ascent of Man");
        title_list.add("The Old Man and The Sea");
        title_list.add("A Portrait of The Artist As a Young Man");
        title_list.add("A Man is a Man but Bubblesort IS A DOG");

        assertEquals(new KWIC().read_title(list), title_list);
    }


    // test the selection of keywords
    @Test
    void test_clean_list() {
        ArrayList<String> list = new ArrayList<>();
        list.add("the");
        list.add("and");
        list.add("of");
        list.add("::");
        list.add("Descent of Man");
        list.add("The Ascent of Man");
        list.add("The Old Man and The Sea");

        ArrayList<String> title_list = new ArrayList<>();
        ArrayList<String> ignore_list = new ArrayList<>();
        title_list = new KWIC().read_title(list);
        ignore_list = new KWIC().read_ignore(list);

        ArrayList<String> key = new ArrayList<>();
        key.add("DESCENT");
        key.add("MAN");
        key.add("ASCENT");
        key.add("MAN");
        key.add("OLD");
        key.add("MAN");
        key.add("SEA");

        assertEquals(new KWIC().clean_data(title_list, ignore_list), key);
    }

    // test the elimination of duplication of keywords
    @Test
    void test_no_duplicate() {
        ArrayList<String> key = new ArrayList<>();
        key.add("DESCENT");
        key.add("MAN");
        key.add("ASCENT");
        key.add("MAN");
        key.add("OLD");
        key.add("MAN");
        key.add("SEA");

        ArrayList<String> no_duplicate = new ArrayList<>();
        no_duplicate.add("DESCENT");
        no_duplicate.add("MAN");
        no_duplicate.add("ASCENT");
        no_duplicate.add("OLD");
        no_duplicate.add("SEA");

        assertEquals(new KWIC().delete_duplicate(key), no_duplicate);
    }

    // test the sorted list of key list
    @Test
    void test_sorted() {
        ArrayList<String> no_duplicate = new ArrayList<>();
        no_duplicate.add("DESCENT");
        no_duplicate.add("MAN");
        no_duplicate.add("ASCENT");
        no_duplicate.add("OLD");
        no_duplicate.add("SEA");

        ArrayList<String> sorted_list = new ArrayList<>();
        sorted_list.add("ASCENT");
        sorted_list.add("DESCENT");
        sorted_list.add("MAN");
        sorted_list.add("OLD");
        sorted_list.add("SEA");

        assertEquals(new KWIC().sort_list(no_duplicate), sorted_list);
    }

    // test how many times a substring shows in a string
    @Test
    void test_num_index() {
        String main = "A Man is a Man but Bubblesort IS A DOG";
        String sub = "Man";
        assertEquals(new KWIC().key_num(main, sub), 2);
    }

    // test the final output
    @Test
    void test_output() {
        ArrayList<String> list = new ArrayList<>();
        list.add("is");
        list.add("the");
        list.add("of");
        list.add("and");
        list.add("as");
        list.add("a");
        list.add("but");
        list.add("::");
        list.add("Descent of Man");
        list.add("The Ascent of Man");
        list.add("The Old Man and The Sea");
        list.add("A Portrait of The Artist As a Young Man");
        list.add("A Man is a Man but Bubblesort IS A DOG");

        ArrayList<String> sample_out = new ArrayList<>();
        sample_out.add("a portrait of the ARTIST as a young man");
        sample_out.add("the ASCENT of man");
        sample_out.add("a man is a man but BUBBLESORT is a dog");
        sample_out.add("DESCENT of man");
        sample_out.add("a man is a man but bubblesort is a DOG");
        sample_out.add("descent of MAN");
        sample_out.add("the ascent of MAN");
        sample_out.add("the old MAN and the sea");
        sample_out.add("a portrait of the artist as a young MAN");
        sample_out.add("a MAN is a man but bubblesort is a dog");
        sample_out.add("a man is a MAN but bubblesort is a dog");
        sample_out.add("the OLD man and the sea");
        sample_out.add("a PORTRAIT of the artist as a young man");
        sample_out.add("the old man and the SEA");
        sample_out.add("a portrait of the artist as a YOUNG man");

        ArrayList<String> title_list = new ArrayList<>();
        ArrayList<String> ignore_list = new ArrayList<>();
        title_list = new KWIC().read_title(list);
        ignore_list = new KWIC().read_ignore(list);

        ArrayList<String> key_list = new ArrayList<String>();
        key_list = new KWIC().clean_data(title_list, ignore_list);

        ArrayList<String> no_duplicate_key = new ArrayList<String>();
        no_duplicate_key = new KWIC().delete_duplicate(key_list);

        ArrayList<String> sorted_list = new ArrayList<String>();
        sorted_list = new KWIC().sort_list(no_duplicate_key);

        assertEquals(new KWIC().output_list(sorted_list, title_list), sample_out);

    }
}
